package main

import (
	"github.com/hamba/avro"
	"log"
)

type LongList struct {
	Value int64
	Next  *LongList
}

func main() {
	schema, err := avro.ParseFiles("linked_list.avsc")
	if err != nil {
		log.Panic(err)
	}

	llist := LongList{
		Value: 1,
		Next: &LongList{
			Value: 2,
			Next: &LongList{
				Value: 3,
				Next:  nil,
			},
		},
	}

	data, err := avro.Marshal(schema, llist)
	if err != nil {
		log.Panic(err)
	}

	print(len(data))
}
